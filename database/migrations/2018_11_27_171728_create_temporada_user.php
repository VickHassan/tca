<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemporadaUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temporada_user', function (Blueprint $table) {
			$table->unsignedInteger('user_id');

            $table->foreign('user_id')
	               ->references('id')
	               ->on('users');
			$table->unsignedInteger('temporada_id');

            $table->foreign('temporada_id')
                 ->references('id')
                 ->on('temporadas');
				$table->primary(['temporada_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temporada_user');
    }
}
