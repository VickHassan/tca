<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horarios', function (Blueprint $table) {
			$table->increments("id");
			$table->unsignedInteger("temporada_id");
			$table->time("horario");
			$table->boolean("status");
			$table->text("dia_da_semana");
			$table->string("sentido", 100);
			$table->timestamps();
			$table->foreign('temporada_id')
                ->references('id')
                ->on('temporadas');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horarios');
    }
}
