<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
			$table->increments('id');
			$table->boolean("status");
			$table->string("tipo");
			$table->text("dia");
            $table->timestamps();

			$table->unsignedInteger("id_usuario");
			$table->unsignedInteger("id_horario");

			$table->foreign("id_usuario")
				->references("id")
				->on("users");

			$table->foreign("id_horario")
				->references("id")
				->on("horarios");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
