<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorarioUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horario_user', function (Blueprint $table) {
			$table->unsignedInteger('horario_id');
			$table->unsignedInteger('user_id');
            $table->foreign('horario_id')
                  ->references('id')
                  ->on('horarios');
			$table->foreign('user_id')
	               ->references('id')
	               ->on('users');
			$table->primary(['horario_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horario_user');
    }
}
