@extends("layouts.app")

@section("content")
	<h1 class="my-4"> Seus Horários </h1>
	<div class="row">
		<div class="col-6">
			<?php $var = 1 ?>
			<h2>Segunda-Feira:</h2>
			@foreach($horarios as $horario)
				@if($horario->dia_da_semana == "segunda")
				
					<div class = "card mb-4 mx-5">	
						<div class = "card-body ">
							<h3 class = "card-title"> Horário N° {{ $var, $var++}}</h3>
							<p  class = "shadow p-3 mb-5 bg-white rounded text-center text-justify">{{ $horario->horario}}</p>
							@if(Auth::user()->adm == 1 )
								<a href="#"class =" btn btn-danger	 mx-2 my-2"> Editar</a>
							@endif
							<a href="" class =" btn btn-dark mx-2">Cadastrar-se </a>
						</div>
					</div>
					@endif
			@endforeach
		</div>
		<div class="col-6">
			<?php $var = 1 ?>
			<h2>Terça-Feira:</h2>
			@foreach($horarios as $horario)
				@if($horario->dia_da_semana == "terca")
							<div class = "card mb-4 mx-5">	
						<div class = "card-body ">
							<h3 class = "card-title"> Horário N° {{ $var, $var++}}</h3>
							<p  class = "shadow p-3 mb-5 bg-white rounded text-center text-justify">{{ $horario->horario}}</p>
							@if(Auth::user()->adm == 1 )
								<a href="#"class =" btn btn-danger	 mx-2 my-2"> Editar</a>
							@endif
							<a href="" class =" btn btn-dark mx-2">Cadastrar-se </a>
						</div>
					</div>
					@endif
			@endforeach
		</div>
		<div class="col-6">
			<?php $var = 1 ?>
			<h2>Quarta-Feira:</h2>
			@foreach($horarios as $horario)
				@if($horario->dia_da_semana == "quarta")
				
					<div class = "card mb-4 mx-5">	
						<div class = "card-body ">
							<h3 class = "card-title"> Horário N° {{ $var, $var++}}</h3>
							<p  class = "shadow p-3 mb-5 bg-white rounded text-center text-justify">{{ $horario->horario}}</p>
							<h4 class="text-align-center">Direção: {{$horario->sentido}}</h4>
							<div class ="row d-flex align-itens-center">
								@if(Auth::user()->adm == 1 )
									<a href="#"class =" btn btn-danger mx-2 my-2"> Editar</a>
								@endif
								<a href="" class =" btn btn-dark mx-2 my-2">Cadastrar-se </a>
							</div>
						</div>
					</div>
					@endif
			@endforeach
		</div>
		<div class="col-6">
			<?php $var = 1 ?>
			<h2>Quinta-Feira:</h2>
			@foreach($horarios as $horario)
				@if($horario->dia_da_semana == "quinta")
							<div class = "card mb-4 mx-5">	
						<div class = "card-body ">
							<h3 class = "card-title"> Horário N° {{ $var, $var++}}</h3>
							<p  class = "shadow p-3 mb-5 bg-white rounded text-center text-justify">{{ $horario->horario}}</p>
							@if(Auth::user()->adm == 1 )
								<a href="#"class =" btn btn-danger	 mx-2 my-2"> Editar</a>
							@endif
							<a href="" class =" btn btn-dark mx-2">Cadastrar-se </a>
						</div>
					</div>
					@endif
			@endforeach
		</div>
		<div class="col-6">
			<?php $var = 1 ?>
			<h2>Sexta-Feira:</h2>
			@foreach($horarios as $horario)
				@if($horario->dia_da_semana == "sexta")
				
					<div class = "card mb-4 mx-5">	
						<div class = "card-body ">
							<h3 class = "card-title"> Horário N° {{ $var, $var++}}</h3>
							<p  class = "shadow p-3 mb-5 bg-white rounded text-center text-justify">{{ $horario->horario}}</p>
							@if(Auth::user()->adm == 1 )
								<a href="#"class =" btn btn-danger	 mx-2 my-2"> Editar</a>
							@endif
							<a href="" class =" btn btn-dark mx-2">Cadastrar-se </a>
						</div>
					</div>
					@endif
			@endforeach
		</div>
	</div>

@endsection
