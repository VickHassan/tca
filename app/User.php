<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $fillable = [
         'name', 'email', 'password', 'telefone', 'adm'
     ];
     /**
      * The attributes that should be hidden for arrays.
      *
      * @var array
      */
     protected $hidden = [
         'password', 'remember_token',
     ];

     public function pedidos(){
       return $this->hasMany(Pedidos::class);
     }

 	public function temporadas()
     {
         return $this->belongsToMany('App\Temporada');
     }

 	public function horarios()
     {
         return $this->belongsToMany('App\Horario');
     }
}
