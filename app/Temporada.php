<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Temporada extends Model
{
	protected $fillable = ['id', 'inicioTemporada', 'fimTemporada', 'descricao', 'status'];

	public function users()
    {
        return $this->belongsToMany('App\User');
    }
	public function horarios()
    {
        return $this->hasMany('App\Horario');
    }
}
