<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedidos extends Model
{
	protected $fillable = ['id', 'status', 'dia', 'tipo', 'id_usuario', 'id_horario'];

	public function users(){
		return $this->belongsTo(User::class);
	}

	public function horario(){
		return $this->belongsTo(Horario::class);
	}
}
