<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
	protected $fillable = ['id', "horario", 'status', 'dia_da_semana', 'sentido', 'temporada_id'];

	public function temporadas()
    {
        return $this->belongsTo(Temporada::class);
    }
	public function pedidos(){
		return $this->hasMany(Pedidos::class);
	}
	public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
