<?php

namespace App\Http\Controllers;

use app\User;
use App\Temporada;
use App\Horario;
use Illuminate\Http\Request;


class UsuarioController extends Controller
{
	function index(){
       $usuarios = User::all();
        return view("usuarios.admin",compact("usuarios", "test"));
    }

	function show(Temporada $temporada){

        $usuarios = $temporada->users;

        return view('usuarios.show', compact('usuarios'));
    }
	function mostrar(Horario $horario){

        $usuarios = $horario->users;

        return view('usuarios.show', compact('usuarios'));
    }
	function conceber(User $usuario){
		$user = User::find($usuario->id);
		$user->adm = 1;
		$user->save();
        return redirect(route('usuarios.admin'));
    }
	function retirar(User $usuario){
		$user = User::find($usuario->id);
 	    $user->adm = 0;
		$user->save();
		return redirect(route('usuarios.admin'));
    }
	public function destroy(User $usuario)
    {
		$usuario = User::findOrFail($usuario->id);

		foreach ($usuario->horarios as $horario)
		{
		   $horario->users()->detach();
		   $horario->save();
		}

		foreach ($usuario->temporadas as $temporada)
		{
		   $temporada->users()->detach();
		   $temporada->save();
		}
        $usuario->delete();
		return redirect(route('index'));
    }
}
