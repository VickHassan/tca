<?php

namespace App\Http\Controllers;
use Auth;
use App\Temporada;
use Illuminate\Http\Request;

class TemporadaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$temporadas = Temporada::all();
        return view('temporadas.index', compact('temporadas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('temporadas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$temporada = Temporada::create([
            'inicioTemporada' => $request['data_inicio'],
            'fimTemporada' => $request['data_fim'],
            'descricao' => $request['descricao'],
            'status' => 1,
        ]);
        return redirect(route('index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Temporada  $temporada
     * @return \Illuminate\Http\Response
     */
    public function show(Temporada $temporada)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Temporada  $temporada
     * @return \Illuminate\Http\Response
     */
    public function edit(Temporada $temporada)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Temporada  $temporada
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Temporada $temporada)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Temporada  $temporada
     * @return \Illuminate\Http\Response
     */
    public function destroy(Temporada $temporada)
    {
		$temporada = Temporada::findOrFail($temporada->id);

		foreach ($temporada->horarios as $horario)
		{
		   $horario->temporadas()->dissociate();
		   $horario->save();
		}

		foreach ($temporada->users as $user)
		{
		   $user->users()->detach();
		   $user->save();
		}
        $usuario->delete();
		return redirect(route('temporada.index'));
    }

	function cadastro(Temporada $temporada) {
        $temporada->users()->attach(Auth::user()->id);
		$temporada->save();
		return redirect(route('temporada.index'));
	}
}
