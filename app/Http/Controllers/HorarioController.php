<?php

namespace App\Http\Controllers;
use Auth;
use App\Horario;
use Illuminate\Http\Request;
use App\Temporada;

class HorarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Temporada $temporada)
    {
		return view('horarios.create', compact('temporada'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Temporada $temporada)
    {
		$horario = Horario::create([
			'temporada_id' => $temporada->id,
            'horario' => $request['horario'],
            'sentido' => $request['sentido'],
            'dia_da_semana' => $request['dia_da_semana'],
            'status' => 1,
        ]);

        return redirect(route('index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Horario  $horario
     * @return \Illuminate\Http\Response
     */
    public function show(Horario $horarios, Temporada $temporada)
    {

		$horarios = Horario::where('temporada_id','=', $temporada->id)->get();
        return view('horarios.show', compact('horarios'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Horario  $horario
     * @return \Illuminate\Http\Response
     */
    public function edit(Horario $horario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Horario  $horario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Horario $horario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Horario  $horario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Horario $horario)
    {
		$horario = Horario::findOrFail($horario->id);

		foreach ($horario->temporadas as $temporada)
		{
		   $temporada->horarios()->dissociate();
		   $temporada->save();
		}

		foreach ($horario->users as $user)
		{
		   $user->horarios()->detach();
		   $user->save();
		}
        $horario->delete();
		return redirect(route('temporada.index'));
    }
	function cadastro(Horario $horario) {
        $horario->users()->attach(Auth::user()->id);
		$horario->save();
		return redirect(route('index'));
	}
}
