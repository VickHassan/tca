@extends("layouts.app")

@section("content")

<h1 class="my-4"> Usuarios </h1>
<div class="row">
	@foreach($usuarios as $usuario)
		<div class = "card mb-4 col-5 mx-5">
			<div class = "card-body ">
				<h2 class = "card-title"> Usuário: {{ $usuario->name }}</h2>
				<p>{{ $usuario->email}}</p>
				<p>{{ $usuario->telefone}}</p>
				@if(Auth::user()->adm == 1 )
					<a href="{{ route('usuarios.destroy', ['usuario' => $usuario]) }}"class =" btn btn-danger	 mx-2 my-2"> Remover</a>
				@endif
			</div>
		</div>

	@endforeach
</div>

@endsection
