@extends('layouts.app')

@section('content')
    <h1 class="my-4"> Dar ou remover administração: </h1>

	<div class="row">
        <table class ="mb-4 col-5 table table-bordered table-striped  text-align-center">
            <thead class="thead-dark">
                <th scope ="col">#</th>
                <th scope="col">Nome</th>
                <th scope= "col">Adicionar</th>
            </thead>
            <tbody>
            <?php $i = 1;?>

                @foreach($usuarios as $usuario)
                    <!--  TEMPORADAS DO SITE  -->
                    @if($usuario->adm == 0)
                        <tr>
                            <th scope ="row"><?= $i; $i++?></th>
                            <td>{{$usuario->name}}</td>
                            <td><a href="{{ route('usuario.conceber', ['usuario' => $usuario]) }}" class =" btn btn-dark my-2 flex-fill mx-1">Adicionar </a></td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>

        <table class ="mb-4 col-5 mx-5 table table-bordered table-striped  text-align-center">
            <thead class="thead-dark">
                <th scope ="col">#</th>
                <th scope="col">Nome</th>
                <th scope= "col">Remover</th>
            </thead>
            <tbody>
            <?php $i = 1;?>

                @foreach($usuarios as $usuario)
                    <!--  TEMPORADAS DO SITE  -->
                    @if($usuario->adm == 1 && Auth::user() != $usuario)
                        <tr>
                            <th scope ="row"><?= $i; $i++?></th>
                            <td>{{$usuario->name}}</td>
                            <td><a href="{{ route('usuario.retirar', ['usuario' => $usuario]) }}" class =" btn btn-dark my-2 flex-fill mx-1">Remover </a></td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
	</div>
@endsection
