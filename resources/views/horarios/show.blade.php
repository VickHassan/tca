@extends("layouts.app")

@section("content")

<h1 class="my-4"> Seus Horários </h1>
<div class="row">
	@foreach($horarios as $horario)
			<div class = "card mb-4 col-5 mx-5">
				<div class = "card-body ">
					<h2 class = "card-title"> Horário N° {{ $horario->id }}</h2>
					<p>{{ $horario->horario}}</p>
					@if(Auth::user()->adm == 1 )
						<a href="{{ route('usuarios.destroy', ['horario' => $horario]) }}"class =" btn btn-danger	 mx-2 my-2"> Remover</a>
						<a href="{{ route('usuarios.mostrar', ['horario' => $horario]) }}" class =" btn btn-dark my-2 flex-fill mx-1">Ver Usuarios </a>

					@else
					<a href="{{ route('horarios.cadastro', ['horario' => $horario]) }}" class =" btn btn-dark mx-2">Cadastrar-se </a>

						<div class="form-group row ">
							<a href="{{ route('pedidos.store', ['horario' => $horario]) }}" type="submit" class =" btn btn-dark my-2 flex-fill mx-1">Fazer Pedido </a>

							<div class="col-md-6 align-self-center align-middle">
								<input type="radio" name="tipo" value="0" checked class="ml-5"> <span class="mx-3" >Cancelar</span>
								<input type="radio" name="tipo" value="1" class="ml-5"><span class="mx-3">Solicitar</span>
							</div>
						</div>
					@endif
				</div>
			</div>

	@endforeach
</div>

@endsection
