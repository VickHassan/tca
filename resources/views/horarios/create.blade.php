@extends("layouts.app")

@section("content")

	<div class="card-header">{{ __('Criar um Horario') }}</div>

	<div class="card-body ">
		<form method="POST" action="{{ route('horarios.store', ['temporada' => $temporada]) }}">
					@csrf
			<div class="form-group row">
				<label for="horario" class="col-sm-4 col-form-label text-md-right">{{ __('Horário') }}</label>

				<div class="col-md-6" >
					<input id="horario" type="time" class="form-control{{ $errors->has('horario') ? ' is-invalid' : '' }} " name="horario" value="{{ old('horario') }}" required autofocus>

					@if ($errors->has('horario'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('horario') }}</strong>
						</span>
					@endif
				</div>
			</div>
			<div class="form-group row ">
				<label class="col-sm-4 col-form-label text-md-right" for="inputGroupSelect01">Dia da semana</label>
				<div class="col-md-6">
					<select class="custom-select" id="inputGroupSelect01" name="dia_da_semana">
						<option selected>Escolha...</option>
						<option value="segunda">Segunda</option>
						<option value="terca">Terça</option>
						<option value="quarta">Quarta</option>
						<option value="quinta">Quinta</option>
						<option value="sexta">Sexta</option>
					</select>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-4 col-form-label text-md-right " for="inputGroupSelect01">Tipo</label>
				<div class="col-md-6 align-self-center align-middle">
					<input type="radio" name="sentido" value="Ida" checked class="ml-5"> <span class="mx-3" >Ida</span>
	 				<input type="radio" name="sentido" value="Vinda" class="ml-5"><span class="mx-3">Volta</span>
				</div>


			</div>
			<div class="form-group row mb-0">
                <div class="col-md-8 offset-md-9">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Registrar') }}
                    </button>
                </div>
            </div>
		</form>
	</div>
@endsection
