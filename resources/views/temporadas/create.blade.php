@extends("layouts.app")

@section("content")
    <div class="card-header">{{ __('Criar uma nova temporada') }}</div>

    <div class="card-body">
        <form method="POST" action="{{ route('temporadas.store') }}">
            @csrf

            <div class="form-group row">
                <label for="data_inicio" class="col-sm-4 col-form-label text-md-right">{{ __('Data Inicio') }}</label>

                <div class="col-md-6">
                    <input id="data_inicio" type="date" class="form-control{{ $errors->has('data_inicio') ? ' is-invalid' : '' }}" name="data_inicio" value="{{ old('data_inicio') }}" required autofocus>

                    @if ($errors->has('data_inicio'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('data_inicio') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="data_fim" class="col-md-4 col-form-label text-md-right">{{ __('Data Fim') }}</label>

                <div class="col-md-6">
                    <input id="data_fim" type="date" class="form-control{{ $errors->has('data_fim') ? ' is-invalid' : '' }}" name="data_fim" required>

                    @if ($errors->has('data_fim'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('data_fim') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="descricao" class="col-sm-4 col-form-label text-md-right">{{ __('Descrição') }}</label>

                <div class="col-md-6">
                    <input id="descricao" type="textarea" class="form-control{{ $errors->has('descricao') ? ' is-invalid' : '' }}" name="descricao" value="{{ old('descricao') }}" required>

                    @if ($errors->has('descricao'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('descricao') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-9">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Registrar') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection
