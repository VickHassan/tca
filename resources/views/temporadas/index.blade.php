@extends('layouts.app')

@section('content')

	@if(Auth::user())
	<input id="filtro" type="text" class="form-control{{ $errors->has('filtro') ? ' is-invalid' : '' }} text-center arredondado" name="filtro" value="{{ old('filtro') }}" placeholder="Pesquisar Usuario">
    <h1 class="my-4"> Suas Temporadas </h1>

	<div class="row temp">
	    @foreach($temporadas as $temporada)
	        <!--  TEMPORADAS DO SITE  -->
		        <div class = "card mb-4 col-5 mx-5">
		            <div class = "card-body ">
		                <h2 class = "card-title"> Temporada {{ $temporada->id }}</h2>
		                <p class="shadow p-3 mb-5 bg-white rounded text-center text-justify">{{ $temporada->descricao}}</p>
						<div class=" align-items-center m-4">
							@if(Auth::user()->adm == 1 )
			                	<a href="{{ route('horarios.create', ['temporada' => $temporada]) }}" class =" btn bg-secondary flex-fill text-white mx-1">Criar Horario</a>
								<a href="{{ route('temporada.destroy', ['temporada' => $temporada]) }}"class =" btn bg-secondary mx-1 my-2 text-white px-3"> Apagar -se</a>
								<a href="{{ route('usuarios.show', ['temporada' => $temporada]) }}"class =" btn bg-secondary  my-2 flex-fill text-white mx-1"> Ver Usuarios</a>
			            	@endif
							@if(Auth::user()->adm == 0 )
								<a href="{{ route('temporada.cadastro', ['temporada' => $temporada]) }}" class =" btn btn-dark my-2 flex-fill mx-1">Cadastrar-se </a>
<<<<<<< HEAD:resources/views/temporadas/index.blade.php
							@endif
							@foreach($temporada->users as $user)

								@if(Auth::user()->id == $user->id || Auth::user()->adm == 1 )

									<a href="{{ route('horarios.show', ['temporada' => $temporada]) }}" class =" btn btn-dark my-2 flex-fill mx-1">Ver horarios </a>
								@endif
							@endforeach

=======
								<a href="" class =" btn btn-dark my-2 flex-fill mx-1">Fazer Pedido </a>
								<a href="{{ action('HorariosController@show') }}" class =" btn btn-dark my-2 flex-fill mx-1">Ver horarios </a>
							@else
							<a href="" class =" btn btn-dark my-2 flex-fill mx-1 d-flex justify-content-center">Fazer Pedido </a>
								<a href="{{ action('HorariosController@show') }}" class =" btn btn-dark my-2 flex-fill mx-1 d-flex justify-content-center">Ver horarios </a>
							@endif

>>>>>>> de0af4ab6cf2093276b29d21a5f5511cfe35c9a7:laravel/resources/views/temporadas/index.blade.php
						</div>
					</div>
		        </div>

	    @endforeach
	</div>
	@else
		<div class="row align-items-center">
			<h1 class="text-center col">BEM VINDO A SUA AGENDA DE VIAGENS <3</h1>
		</div>

	@endif
@endsection
