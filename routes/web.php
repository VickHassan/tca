<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

route::get('/temporadas', 'TemporadaController@index')
    ->name("temporadas.index");
route::get("/temporadas/novo", 'TemporadaController@create')
    ->middleware('auth')
    ->name('temporadas.create');
route::post('/temporadas', 'TemporadaController@store')
    ->middleware('auth')
    ->name('temporadas.store');
route::get("/temporadas/{temporada}", 'TemporadaController@cadastro')
	->name('temporada.cadastro');
route::get("/temporada/deletar/{temporada}", "TemporadaController@destroy")
    ->name("temporada.destroy");

route::get("/horario/{temporada}", 'HorarioController@show')
    ->name('horarios.show');
route::get("/horarios/{temporada}", 'HorarioController@create')
    ->name('horarios.create');
route::post("/horarios/{temporada}", 'HorarioController@store')
	 ->name('horarios.store');
 route::get("/horario/cadastro/{horario}", 'HorarioController@cadastro')
 	->name('horarios.cadastro');
route::get("/horario/deletar/{horario}", "HorarioController@destroy")
    ->name("horario.destroy");


route::post('/pedido/{horario}', 'PedidosController@store')
    ->middleware('auth')
    ->name('pedidos.store');

route::get("/admin", "UsuarioController@index")
    ->middleware('auth')
    ->name("usuarios.admin");
route::get("/admin/retirar/{usuario}", "UsuarioController@retirar")
    ->middleware('auth')
    ->name("usuario.retirar");
route::get("/admin/conceber/{usuario}", "UsuarioController@conceber")
    ->middleware('auth')
    ->name("usuario.conceber");
route::get("/admin/deletar/{usuario}", "UsuarioController@destroy")
    ->middleware('auth')
    ->name("usuarios.destroy");
route::get("/usuario/{temporada}", 'UsuarioController@show')
		->middleware("auth")
		->name('usuarios.show');
route::get("/usuario/horarios/{horario}", 'UsuarioController@mostrar')
		->middleware("auth")
		->name('usuarios.mostrar');
route::get('/', 'TemporadaController@index')
    ->name('index');

Auth::routes();
